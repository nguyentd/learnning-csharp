using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace T3_P3._5._1
{
    class Program : Form
    {
        public Program()
        {
            CenterToScreen();
            this.Text = "Basic Main Form";
            this.Paint += new PaintEventHandler(MainForm_paint);
        }

        private void MainForm_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawString("Hello GDI+", new Font("Time New Roman", 25), new SolidBrush(Color.Black), 0, 0);
            Pen pen = new Pen(Color.Red, 15);
            g.DrawArc(pen, 50, 20, 100, 200,40, 210);
        }

        static void Main(string[] args)
        {
            Application.Run(new Program());
            //Console.ReadKey();

        }
    }
}
