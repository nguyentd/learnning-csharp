﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1_9._2
{
    interface ILoggedCompressible: ICompressible
    {
        void LogSaveBytes();
    }
}
