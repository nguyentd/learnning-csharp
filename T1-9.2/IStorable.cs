﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1_9._2
{
    interface IStorable
    {
        void Read();
        void Write(object obj);
        int Status { get; set; }
    }
}
