using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_9._2
{
    class Program
    {
        static void Main(string[] args)
        {

            Document doc = new Document("Test Document");

            IStorable iDoc = doc as IStorable;
            if (iDoc != null)
            {
                iDoc.Read();
            }
            else
            {
                Console.WriteLine("IStorable is not supported");
            }
            ILoggedCompressible iLog = doc as ILoggedCompressible;
            if(iLog != null)
            {
                iLog.LogSaveBytes();
                iLog.Compress();
            }
            else
            {
                Console.WriteLine("ILoggedCompressible is not supported");
            }
            
            Console.ReadKey();

            
        }
    }
}
