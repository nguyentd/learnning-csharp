﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1_9._2
{
    class Document : IStorableCompressible, IEncryptable
    {
        public Document(string s)
        {
            Console.WriteLine("Create Document: {0}", s);
        }
  
        public int Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public void Compress()
        {
            Console.WriteLine("Compress");
        }

        public void Decompress()
        {
            Console.WriteLine("Decompress");
        }

        public void Decrypt()
        {
            Console.WriteLine("Decrypt");
        }

        public void Encrypt()
        {
            Console.WriteLine("Encrypt");
        }

        public void LogOriginalSize()
        {
            Console.WriteLine("LogOriginalSize");
        }

        public void LogSaveBytes()
        {
            Console.WriteLine("LogSaveBytes");
        }

        public void Read()
        {
            Console.WriteLine("Read() for IStorable");
        }

        public void Write(object obj)
        {
            Console.WriteLine("Write() for IStorable");
        }

        private int status = 0;
    }
}
