using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace T2_1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo(@"C:\Windows\");
            Console.WriteLine("-Dirinfo-");
            Console.WriteLine("Full name: {0}", dir.FullName);
            Console.WriteLine("Name: {0}", dir.Name);
            Console.WriteLine("Parent: {0}", dir.Parent);
            Console.WriteLine("Create at: {0}", dir.CreationTime);
            Console.WriteLine("Attribute: {0}", dir.Attributes.ToString());
            Console.WriteLine("Root: {0}", dir.Root);

            FileInfo[] files = dir.GetFiles(searchPattern: "*.exe");
            Console.WriteLine("Found {0} exe files", files.Length);

            foreach(FileInfo f in files)
            {
                Console.WriteLine("-------------------------------");
                Console.WriteLine("Name: {0}", f.Name);
                Console.WriteLine("Size: {0}", f.Length);
                Console.WriteLine("Creation time: {0}\n", f.CreationTime);
            }

            Console.ReadKey();

        }
    }
}
