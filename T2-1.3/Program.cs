using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace T2_1._3
{
    class Program
    {
        static void Main(string[] args)
        {

            Program p = new Program();
            string path = @"D:\xin\phan mem";
            DirectoryInfo dir = new DirectoryInfo(path);
            p.ExplorerDir(dir);

            Console.WriteLine("\n\n{0} directories found. \n", dirCounter);

            Console.ReadKey();
        }

        private void ExplorerDir(DirectoryInfo info)
        {
            indentLevel++;
            for(int i = 0; i< indentLevel; i++)
            {
                Console.Write("  ");
            }
            Console.WriteLine("[{0}] {1} [{2}]\n", indentLevel, info.Name, info.LastAccessTime);

            DirectoryInfo[] directories = info.GetDirectories();
            foreach(DirectoryInfo d in directories)
            {
                dirCounter++;
                ExplorerDir(d);
            }
            indentLevel--;
        }

        static int indentLevel = -1;
        static int dirCounter = -1;
    }
}
