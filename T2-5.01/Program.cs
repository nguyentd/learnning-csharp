using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace T2_5._01
{
    class Program
    {
        static void Main(string[] args)
        {
            MessageBox.Show("Loaded Windows Form dll.");
            AppDomain appDomain = AppDomain.CurrentDomain;
            Assembly[] loadedAs = appDomain.GetAssemblies();
            Console.WriteLine("Loaded Assemblies.");
            foreach(Assembly a in loadedAs)
            {
                Console.WriteLine(a.FullName);
            }
            Console.ReadKey();

        }
    }
}
