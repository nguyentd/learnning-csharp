using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_13._3
{
    class MyClassWithDelegate
    {
        public delegate void StringDelegate(string s);
    }

    class MyImplementingClass
    {
        public static void WriteString(string s)
        {
            Console.WriteLine("Writing string: {0}", s);
        }
        public static void LogString(string s)
        {
            Console.WriteLine("Logging string: {0}", s);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            MyClassWithDelegate.StringDelegate Writer, Logger;
            MyClassWithDelegate.StringDelegate multiCastDelegate;

            Writer = new MyClassWithDelegate.StringDelegate(MyImplementingClass.WriteString);
            Logger = new MyClassWithDelegate.StringDelegate(MyImplementingClass.LogString);

            Writer("Chuoi qua Writer");
            Logger("Chuoi qua Logger");

            multiCastDelegate = Writer + Logger;

            multiCastDelegate("Chuoi qua Multicast");
            Console.ReadKey();

            
        }
    }
}
