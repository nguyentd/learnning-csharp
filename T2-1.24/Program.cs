using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace T2_1._24
{
    [Serializable]
    class Program
    {
        static void Main(string[] args)
        {
           
            Console.WriteLine("Creating first one with new...");
            Program app = new Program(1,10);
            Console.WriteLine("Creating second one with deseializable...");
            Program app2 = Program.Deserilize();
            app2.DisplaySum();
            Console.ReadKey();
        }

        public Program(int start, int end)
        {
            this.start = start;
            this.end = end;
            ComputeSum();
            DisplaySum();
            Serialize();
        }

        private void Serialize()
        {
            Console.WriteLine("Serializing...");
            FileStream fileStream = new FileStream("db.dat",FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, this);
            Console.WriteLine("Completed.");
            fileStream.Close();
        }

        private void DisplaySum()
        {
            foreach(int i in sum)
            {
                Console.WriteLine("i: {0}", i);
            }
        }

        private void ComputeSum()
        {
            int count = end - start + 1;
            sum = new int[count];
            sum[0] = start;
            for (int i = 1, j = start + 1; i < count; i++,j++)
            {
                sum[i] = j + sum[i - 1];
            }
        }

        private static Program Deserilize()
        {
            FileStream fileStream = new FileStream("db.dat", FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            Program temp = (Program)binaryFormatter.Deserialize(fileStream);
            fileStream.Close();
            return temp;

        }

        private int start = 1;
        private int end;
        private int[] sum;
    }
}
