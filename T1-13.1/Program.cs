using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_13._1
{
    public enum comparison
    {
        theFirst = 1,
        theSecond = 2
    }

    public class Pair
    {
        public delegate comparison WichIsFirst(object obj1, object obj2);

        public Pair(Object o1, object o2)
        {
            thePair[0] = o1;
            thePair[1] = o2;
        }

        public void Sort(WichIsFirst cb)
        {
            if (cb(thePair[0],thePair[1]) == comparison.theSecond)
            {
                object temp = thePair[0];
                thePair[0] = thePair[1];
                thePair[1] = temp;
            }
        }

        public override string ToString()
        {
            return thePair[0].ToString() + ", " + thePair[1].ToString();
        }
        private object[] thePair = new object[2];
    }
    class Program
    {
        static comparison myCb(object obj1, object obj2)
        {
            if ((int)obj1 >= (int)obj2)
                return comparison.theFirst;
            else
                return comparison.theSecond;
        }
        static void Main(string[] args)
        {

            Pair p1 = new Pair(4, 5);
            Console.WriteLine("Pair: {0}", p1);

            Pair.WichIsFirst wichIsFirst = new Pair.WichIsFirst(myCb);

            p1.Sort(wichIsFirst);

            Console.WriteLine("Pair: {0}", p1);
            Console.ReadKey();

            
        }
    }
}
