﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1_6._6
{
    class Fraction
    {

        public Fraction(int numer, int denom)
        {
            this.numer = numer;
            this.denom = denom;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(" {0}/{1}", numer, denom);
            return sb.ToString();
        }

        internal class FractionArtist
        {
            public void Draw(Fraction f)
            {
                Console.WriteLine("numerator: {0}", f.numer);
                Console.WriteLine("denominator: {0}", f.denom);

            }
        }


        private int numer;
        private int denom;
    }
}
