using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_6._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Fraction f = new Fraction(3, 4);
            Console.WriteLine("f: {0}", f);

            Fraction.FractionArtist fractionArtist = new Fraction.FractionArtist();
            fractionArtist.Draw(f);
           
            Console.ReadKey();
        }
    }
}
