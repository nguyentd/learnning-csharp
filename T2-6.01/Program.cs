using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace T2_6._01
{
    class Program
    {
        static void Main(string[] args)
        {

            Program app = new Program();
            app.DoTest();
            Console.ReadKey();

        }

        public void DoTest()
        {
            Thread t1 = new Thread(new ThreadStart(Incre));
            Thread t2 = new Thread(new ThreadStart(Decre));
            t1.Start();
            t2.Start();
        }

        public void Incre()
        {
            Console.WriteLine("\n");
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine("Incre: {0}", i);
            }
        }

        public void Decre()
        {
            Console.WriteLine("\n");
            for (int i = 10; i >= 0; i--)
            {
                Console.WriteLine("Decre: {0}", i);
            }
        }
    }
}
