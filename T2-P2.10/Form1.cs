using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace T2_P2._10
{
    public partial class Form1 : Form
    {
        public class FileComparer: IComparer
        {
            public int Compare(object f1, object f2)
            {
                FileInfo file1 = (FileInfo)f1;
                FileInfo file2 = (FileInfo)f2;
                if (file1.Length > file2.Length)
                    return -1;
                else if (file1.Length < file2.Length)
                    return 1;
                else
                    return 0;
            }
        }

        public Form1()
        {
            InitializeComponent();
            //FillDirTree(tvwSource, true);
            //FillDirTree(tvwTargetDir, false);
            lblStatus.Text = "";
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FillDirTree(TreeView treeView, bool isSource)
        {
            treeView.Nodes.Clear();
            string[] strDriver = Environment.GetLogicalDrives();
      
            foreach(string rootDirName in strDriver)
            {
                if (rootDirName.ToString() != "D:\\")
                    continue;
                try
                {
                    DirectoryInfo dir = new DirectoryInfo(rootDirName);
                    dir.GetDirectories();

                    TreeNode nRoot = new TreeNode(rootDirName);
                    treeView.Nodes.Add(nRoot);
                    if (isSource)
                        GetSubDirNodes(nRoot, nRoot.Text, true);
                    else
                        GetSubDirNodes(nRoot, nRoot.Text, false);

                }catch(Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        private void GetSubDirNodes(TreeNode parenNode, string fullName, bool getFile)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(fullName);
            DirectoryInfo[] dirSub = directoryInfo.GetDirectories();
            foreach(DirectoryInfo dir in dirSub)
            {
                if((dir.Attributes & FileAttributes.Hidden) != 0)
                {
                    continue;
                }
                TreeNode subNode = new TreeNode(dir.Name);
                parenNode.Nodes.Add(subNode);
                GetSubDirNodes(subNode, dir.FullName, getFile);

            }
            if (getFile)
            {
                FileInfo[] files = directoryInfo.GetFiles();
                foreach(FileInfo f in files)
                {
                    TreeNode tree = new TreeNode(f.Name);
                    parenNode.Nodes.Add(tree);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            foreach(TreeNode node in tvwSource.Nodes)
            {
                SetCheck(node, false);
            }
        }

        private void SetCheck(TreeNode node, bool v)
        {
            //node.Checked = v;
            foreach (TreeNode n in node.Nodes)
            {
                if(node.Nodes.Count == 0)
                {
                    node.Checked = v;
                }
                else
                {
                    SetCheck(n, v);
                }
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            ArrayList fileList = GetFileList();
            foreach(FileInfo file in fileList)
            {
                try
                {
                    lblStatus.Text = "Copying " + txtTargetDir.Text + "\\" + file.Name + "...";
                    Application.DoEvents();
                    file.CopyTo(txtTargetDir.Text + "\\" + file.Name, chkOverwrite.Checked);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                lblStatus.Text = "Done";
                Application.DoEvents();
            }

        }

        private ArrayList GetFileList()
        {
            ArrayList fileName = new ArrayList();
            ArrayList fileList = new ArrayList();
            IComparer comparer = new FileComparer();
            foreach (TreeNode tvNode in tvwSource.Nodes)
            {
                GetCheckedFile(tvNode, fileName);
            }

            foreach(string file in fileName)
            {
                FileInfo f = new FileInfo(file);
                if (f.Exists)
                    fileList.Add(f);
            }

            fileList.Sort(comparer);
            return fileList;
        }

        private void GetCheckedFile(TreeNode tvNode, ArrayList fileName)
        {
            if (tvNode.Nodes.Count == 0)
            {
                if (tvNode.Checked)
                {
                    string fullPath = GetParentString(tvNode);
                    fileName.Add(fullPath);
                }
                
            }
            else
            {
                foreach (TreeNode n in tvNode.Nodes)
                {
                    GetCheckedFile(n, fileName);
                }
            }
        }

        private void tvwSource_AfterCheck(object sender, TreeViewEventArgs e)
        {
            SetCheck(e.Node, e.Node.Checked);
        }

        private void tvwTargetDir_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string fullPath = GetParentString(e.Node);
            if (fullPath.EndsWith("\\"))
                fullPath = fullPath.Substring(0, fullPath.Length - 1);
            txtTargetDir.Text = fullPath;
        }

        private string GetParentString(TreeNode node)
        {
            if(node.Parent == null)
            {
                return node.Text;
            }
            else
            {
                return GetParentString(node.Parent) + node.Text + (node.Nodes.Count == 0 ? "" : "\\");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you quite sure?", "Delete File", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (dialogResult == DialogResult.OK)
            {
                ArrayList fileNames = GetFileList();
                foreach (FileInfo f in fileNames)
                {
                    try
                    {


                        lblStatus.Text = "Deleting " + txtTargetDir.Text + "\\" + f.Name + "...";
                        Application.DoEvents();
                        f.Delete();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
                lblStatus.Text = "Done.";
                Application.DoEvents();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillDirTree(tvwSource, true);
            FillDirTree(tvwTargetDir, false);
        }
    }
}
