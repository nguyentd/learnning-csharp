using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace T2_1._8
{
    class Program
    {
        static void Main(string[] args)
        {

            Stream input = File.OpenRead("test1.txt");
            Stream output = File.OpenWrite("test2.txt");
            byte[] buf = new Byte[1];
            int byteRead;
            while((byteRead = input.Read(buf,0,1))>0)
            {
                output.Write(buf, 0, byteRead);
            }

            input.Close();
            output.Close();
            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}
