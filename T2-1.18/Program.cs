using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace T2_1._18
{
    class Program
    {
        static void Main(string[] args)
        {

            TcpClient client;

            try
            {
                client = new TcpClient("localhost", 65000);

            }
            catch
            {
                Console.WriteLine("Failed to connect.");
                Console.ReadKey();
                return;
            }

            NetworkStream networkStream = client.GetStream();
            System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
            string s = streamReader.ReadLine();
            Console.WriteLine(s);

            streamReader.Close();
            networkStream.Close();
            client.Close();

            Console.ReadKey();
        }
    }
}
