using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_9._6
{

    interface IStorable
    {
        void Read();
        void Write();
    }
    interface ITalk
    {
        void Read();
        void Talk();
    }

    class Program: IStorable, ITalk
    {
        public Program(string s)
        {
            Console.WriteLine("Tao documment voi: {0}", s);
        }


        static void Main(string[] args)
        {
            Program pg = new Program("Test Document");

            IStorable idoc = pg as IStorable;
            if (idoc != null)
            {
                idoc.Read();
            }
            Console.WriteLine();

            ITalk it = pg as ITalk;
            if (it != null)
            {
                it.Read();
            }
            Console.WriteLine();
            pg.Read();
            pg.Talk();

            Console.ReadKey();

        }

        public void Read()
        {
            Console.WriteLine("Read() cua IStorable");
        }

        public void Talk()
        {
            Console.WriteLine("Talk() cua ITalk");
        }

        public void Write()
        {
            Console.WriteLine("Write() cua IStorable");
        }

        void ITalk.Read()
        {
            Console.WriteLine("Read() cua ITalk");
        }
    }
}
