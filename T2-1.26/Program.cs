using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.IsolatedStorage;

namespace T2_1._26
{
    class Program
    {
        static void Main(string[] args)
        {


            IsolatedStorageFileStream cfg = new IsolatedStorageFileStream("Nguyen.cfg", FileMode.Create);
            StreamWriter streamWriter = new StreamWriter(cfg);
            string outout;
            System.DateTime curr = System.DateTime.Now;
            outout = "Last Access: " + curr.ToString();
            streamWriter.WriteLine(outout);
            outout = "Last position = 27.35";
            streamWriter.WriteLine(outout);
            streamWriter.Flush();
            streamWriter.Close();
            
            cfg.Close();
            Console.ReadKey();

        }
    }
}
