﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_6._2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyWindow[] myArr = new MyWindow[3];
            myArr[0] = new MyWindow(1, 2);
            myArr[1] = new ListBox(2, 3, "Caption");
            myArr[2] = new Button(3, 4);

            for(int i = 0; i<3; i++)
            {
                myArr[i].Draw();
            }
            Console.Read();
        }
    }
}
