using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace T2_1._17
{
    class Program
    {


        static void Main(string[] args)
        {
            Program ap = new Program();
            ap.Run();
            
            Console.ReadKey();

           
        }

        private void Run()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            TcpListener tcpListener = new TcpListener(65000);
#pragma warning restore CS0618 // Type or member is obsolete

            tcpListener.Start();

            for(; ; )
            {
                Socket client = tcpListener.AcceptSocket();
                if (client.Connected)
                {
                    Console.WriteLine("Client connected.");
                    SendFileToClient(client);
                    Console.WriteLine("Disconnected.");
                    client.Close();
                    //break;
                }
            }


        }

        private void SendFileToClient(Socket client)
        {
            NetworkStream networkStream = new NetworkStream(client);
            System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);
            streamWriter.WriteLine("a b c d e f g h i");
            streamWriter.Flush();

            streamWriter.Close();
            networkStream.Close();
        }
    }
}
