using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace T2_7._02
{
    class Program
    {
        [DllImport("user32", ExactSpelling = true, CharSet = CharSet.Unicode, EntryPoint = "MessageBoxW")]
        public static extern int DisplayMessage(int hWnd, string pText, string pCaption, int uType);
        

        static void Main(string[] args)
        {
            String pText = "Xin chao ba con.";
            String pCaption = "Invoke Test";
            DisplayMessage(0, pText, pCaption, 0);
            Console.ReadKey();

        }
    }
}
