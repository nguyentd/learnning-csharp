using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace T2_1._16
{
    class Program
    {
        private Stream input;
        private byte[] buf;
        const int buf_size = 20480;
        private AsyncCallback myCb;

        public Program()
        {
            input = File.OpenRead("test.txt");
            buf = new byte[buf_size];
            myCb = new AsyncCallback(this.OnCompleteRead);

        }

        void Run()
        {
            input.BeginRead(buf, 0, buf.Length, myCb,null);
            for(long i = 0; i < 500000; i++)
            {
                if(i % 100 == 0)
                {
                    Console.WriteLine("i: {0}", i);
                }
            }
        }

        void OnCompleteRead(IAsyncResult asyncResult)
        {
            int byRead = input.EndRead(asyncResult);
            if (byRead > 0)
            {
                string s = Encoding.ASCII.GetString(buf, 0, byRead);
                Console.WriteLine(s);
                input.BeginRead(buf, 0, buf.Length, myCb,null);
            }
        }
        static void Main(string[] args)
        {
            Program app = new Program();
            app.Run();
            Console.ReadKey();
        }
    }
}
