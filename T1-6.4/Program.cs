using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_6._4
{
    class Program
    {
        static void Main(string[] args)
        {
            SomeClass scl = new SomeClass(5);
            Console.WriteLine("{0}", scl);
            Console.ReadKey();
        }
    }

    class SomeClass
    {
        public SomeClass(int val)
        {
            this.val = val;
        }

        public override string ToString()
        {
            return val.ToString();
        }
        private int val;
    }
}
