using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T1_6._3
{
    class Program
    {
        static void Main(string[] args)
        {
            MyWindow[] myArr = new MyWindow[2];
            myArr[0] = new ListBox(2, 3, "Caption");
            myArr[1] = new Button(3, 4);

            for (int i = 0; i < 2; i++)
            {
                myArr[i].Draw();
            }
            Console.Read();
        }
    }
}
