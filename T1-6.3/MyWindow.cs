﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1_6._3
{
    abstract class MyWindow
    {

        protected int top;
        protected int left;

        public MyWindow(int top, int left)
        {
            this.top = top;
            this.left = left;
        }
        public abstract void Draw();
    }

    class ListBox : MyWindow
    {

        private string content;
        public ListBox(int top, int left, string content) : base(top, left)
        {
            this.content = content;
        }
        public override void Draw()
        {
            Console.WriteLine("ListBox: conten la:{0}", content);
        }
    }

    class Button : MyWindow
    {
        public Button(int top, int left) : base(top, left) { }
        public override void Draw()
        {
            Console.WriteLine("Button: ve tai {0},{1}", top, left);
        }
    }
}
