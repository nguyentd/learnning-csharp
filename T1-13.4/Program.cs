using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace T1_13._4
{
    class TimeFinfoEventArgs: EventArgs
    {
        public TimeFinfoEventArgs(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public readonly int hour;
        public readonly int minute;
        public readonly int second;
    }

    class Clock
    {
        public delegate void SecondChangeHandler(object clock, TimeFinfoEventArgs timeFinfo);

        public event SecondChangeHandler onSecondChange;

        public void Run()
        {
            
            for(; ; )
            {
                Thread.Sleep(10);
                DateTime dt = DateTime.Now;
                if (dt.Second != second)
                {
                    TimeFinfoEventArgs timeFinfomation = new TimeFinfoEventArgs(dt.Hour, dt.Minute, dt.Second);
                    if (onSecondChange != null)
                    {
                        onSecondChange(this, timeFinfomation);
                    }
                    this.hour = dt.Hour;
                    this.second = dt.Second;
                    this.minute = dt.Minute;
                }
            }

        }
        private int hour;
        private int minute;
        private int second;
    }


    class DisplayClock
    {
        public void Subcribe(Clock aClock)
        {
            aClock.onSecondChange += new Clock.SecondChangeHandler(TimeHasChange);

        }
        public void TimeHasChange(object sender, EventArgs eventArgs)
        {
            TimeFinfoEventArgs ti = eventArgs as TimeFinfoEventArgs;
            Console.WriteLine("Thoi gian hien hanh: {0}, {1}, {2}", ti.hour, ti.minute, ti.second);

        }
    }

    class LogCurrentTime
    {
        public void Subcribe(Clock aClock)
        {
            aClock.onSecondChange += new Clock.SecondChangeHandler(WriteLogEntry);
        }
        public void WriteLogEntry(object sender, EventArgs eventArgs)
        {
            TimeFinfoEventArgs ti = eventArgs as TimeFinfoEventArgs;
            Console.WriteLine("Ghi theo doi len file: {0}, {1}, {2}", ti.hour, ti.minute, ti.second);

        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            Clock theClock = new Clock();
            DisplayClock displayClock = new DisplayClock();
            displayClock.Subcribe(theClock);

            LogCurrentTime logCurrent = new LogCurrentTime();
            logCurrent.Subcribe(theClock);

            theClock.Run();
            Console.ReadKey();

           
        }
    }
}
