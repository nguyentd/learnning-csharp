using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace T3_P3._5._4
{
    class Program: Form
    {
        private Container compoment = null;
        private ArrayList myPts = new ArrayList();

        public Program()
        {
            InitializeCompoment();
            CenterToScreen();
            this.Text = "Basic Paint Form";
            this.Paint += new PaintEventHandler(this.Main_paint);
        }

        private void Main_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawString("Hello GDI+", new Font("Time New Roman", 25),
                    new SolidBrush(Color.Black), 0, 0);
            Pen drawingPen = new Pen(Color.Red, 15);
            e.Graphics.DrawArc(drawingPen, 50, 20, 100, 200, 40, 210);
            foreach (Point p in myPts)
                g.DrawEllipse(new Pen(Color.Green), p.X, p.Y, 10, 10);
        }

        private void InitializeCompoment()
        {
            this.AutoScaleBaseSize = new Size(5, 13);
            this.ClientSize = new Size(292, 273);
            this.Name = "MainForm";
            this.Text = "From 1";
            this.MouseDown += new MouseEventHandler(Form_mouse);
            this.Paint += new PaintEventHandler(this.Main_paint);
        }

        private void Form_mouse(object sender, MouseEventArgs e)
        {
            Graphics g = Graphics.FromHwnd(this.Handle);
            g.DrawEllipse(new Pen(Color.Green), e.X, e.Y, 10, 10);
            myPts.Add(new Point(e.X, e.Y));
            Invalidate();
        }

        static void Main(string[] args)
        {
            Application.Run(new Program());
        }
    }
}
